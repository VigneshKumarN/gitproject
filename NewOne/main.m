//
//  main.m
//  NewOne
//
//  Created by Orbgen2 on 08/07/17.
//  Copyright © 2017 Orbgen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
