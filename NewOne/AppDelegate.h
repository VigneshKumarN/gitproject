//
//  AppDelegate.h
//  NewOne
//
//  Created by Orbgen2 on 08/07/17.
//  Copyright © 2017 Orbgen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

